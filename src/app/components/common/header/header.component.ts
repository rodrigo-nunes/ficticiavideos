import { Component, OnInit, Input } from '@angular/core';
import { HeaderElements } from './header.elements';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private El: HeaderElements;

  searchTerm = '';

  constructor(private router: Router) {}

  ngOnInit() {
    this.El = new HeaderElements();
  }

  toggleInputSearch() {
    const windowWidth = window.innerWidth;

    if (windowWidth >= 768) {
      this.El.search.classList.toggle('is--active');
      this.El.searchInput.classList.toggle('is--active');

      if (this.El.searchInput.classList.contains('is--active')) {
        this.El.searchInput.focus();
      }
    } else {
      this.El.searchMobile.classList.toggle('is--active');
    }

    this.clearInputSearch();
  }

  doSearch() {
    this.router.navigate(['/busca'], {
      queryParams: { 'q': this.searchTerm }
    });

    this.clearInputSearch();
  }

  onKey(event) {
    if (event.keyCode === 13) {
      this.doSearch();
    }
  }

  private clearInputSearch() {
    this.El.searchInput.nodeValue = '';
    this.El.searchMobileInput.nodeValue = '';
    this.searchTerm = '';
  }

}
