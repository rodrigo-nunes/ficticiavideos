export class HeaderElements {

  private _self: HTMLElement;
  private _search: HTMLElement;
  private _searchButton: HTMLElement;
  private _searchInput: HTMLElement;
  private _searchMobile: HTMLElement;
  private _searchMobileInput: HTMLElement;

  constructor () {
    this._self = document.querySelector('.js--header');
    this._search = document.querySelector('.js--header-search');
    this._searchButton = document.querySelector('.js--header-search-button');
    this._searchInput = document.querySelector('.js--header-search-input');
    this._searchMobile = document.querySelector('.js--header-search-mobile');
    this._searchMobileInput = document.querySelector('.js--header-search-mobile-input');
  }

  get self() {
    return this._self;
  }

  get search() {
    return this._search;
  }

  get searchButton() {
    return this._searchButton;
  }

  get searchInput() {
    return this._searchInput;
  }

  get searchMobile() {
    return this._searchMobile;
  }

  get searchMobileInput() {
    return this._searchMobileInput;
  }

}
