export class VideosElements {

  private _self: HTMLElement;
  private _container: HTMLElement;
  private _loader: HTMLElement;
  private _gridItems: HTMLElement;
  private _gridButton: HTMLElement;
  private _gridLoading: HTMLElement;

  constructor() {
    this._self = document.querySelector('.js--videos');
    this._container = document.querySelector('.js--videos-container');
    this._loader = document.querySelector('.js--videos-loader');
    this._gridItems = document.querySelector('.js--videos-grid-items');
    this._gridButton = document.querySelector('.js--videos-grid-button');
    this._gridLoading = document.querySelector('.js--videos-grid-loading');
  }

  get self() {
    return this._self;
  }

  get container() {
    return this._container;
  }

  get loader() {
    return this._loader;
  }

  get gridItems() {
    return this._gridItems;
  }

  get gridButton() {
    return this._gridButton;
  }

  get gridLoading() {
    return this._gridLoading;
  }
}
