import { Component, OnInit } from '@angular/core';
import { VideosElements } from './videos.elements';
import { YoutubeService } from 'src/app/core/youtube/youtube.service';
import { ModalService } from 'src/app/core/modal/modal.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})

export class VideosComponent implements OnInit {

  private El: VideosElements;
  private channelName = 'TEDtalksDirector';
  private nextPageToken: string;

  videos = {
    dataVideos: [],
    loading: false
  }

  constructor(private youtubeService: YoutubeService, private modalService: ModalService) { }

  ngOnInit() {
    this.El = new VideosElements();
    this.getVideos();
  }

  loadMoreVideos() {
    this.El.gridButton.classList.add('is--hidden');
    this.El.gridLoading.classList.add('is--active');

    this.getVideos(12, false);
  }

  openModal(video: any) {
    this.modalService.build(video);
  }

  private getVideos(maxResults: number = 12, loader: boolean = true) {
    if (loader) {
      this.videos.loading = true;
    }

    this.youtubeService.getVideos(this.channelName, maxResults, this.nextPageToken)
    .then(data => {
      this.nextPageToken = data.nextPageToken;

      data.items.forEach(video => {
        this.videos.dataVideos.push(video);
      });

      if (this.El.gridLoading.classList.contains('is--active')) {
        this.El.gridButton.classList.remove('is--hidden');
        this.El.gridLoading.classList.remove('is--active');
      }

      if (loader) {
        this.videos.loading = false;
      }
    });
  }
}
