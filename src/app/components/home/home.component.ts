import { Component, OnInit } from '@angular/core';
import { HomeElements } from './home.elements';
import { YoutubeService } from 'src/app/core/youtube/youtube.service';
import { HelpersService } from 'src/app/core/helpers/helpers.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  private El: HomeElements;
  private channelName = 'TEDtalksDirector';
  private nextPageToken: string;

  home = {
    dataVideos: [],
    spotlight: null,
    loading: false
  }

  constructor(private youtubeService: YoutubeService, private helpersService: HelpersService) { }

  ngOnInit() {
    this.El = new HomeElements();
    this.getVideos();
  }

  setSpotlight(video: any) {
    const date = this.helpersService.getDateFull(video.snippet.publishedAt);
    const views = this.helpersService.getFormattedNumber(video.statistics.viewCount);
    const iframe = this.helpersService.createIframe(video.id);

    this.clearSpotlight();

    this.El.spotlightVideo.appendChild(iframe);
    this.El.spotlightTitle.innerText = video.snippet.title;
    this.El.spotlightViews.innerText = `${views} views`;
    this.El.spotlightDate.innerText = date;
    this.El.spotlightDescription.innerText = video.snippet.description;
  }

  showDescription() {
    this.El.spotlightInfo.classList.toggle('is--active');
    this.El.spotlightDescription.classList.toggle('is--active');
  }

  loadMoreVideos() {
    this.El.videosButton.classList.add('is--hidden');
    this.El.videosLoading.classList.add('is--active');

    this.getVideos(10, false);
  }

  private getVideos(maxResults: number = 5, loader: boolean = true) {
    if (loader) {
      this.home.loading = true;
    }

    this.youtubeService.getVideos(this.channelName, maxResults, this.nextPageToken, '', 'relevance')
    .then(data => {
      this.nextPageToken = data.nextPageToken;

      data.items.forEach(video => {
        this.home.dataVideos.push(video);
      });

      if (!this.home.spotlight) {
        this.home.spotlight = this.home.dataVideos.splice(0, 1)[0];
        this.setSpotlight(this.home.spotlight);
      }

      if (this.El.videosLoading.classList.contains('is--active')) {
        this.El.videosButton.classList.remove('is--hidden');
        this.El.videosLoading.classList.remove('is--active');
      }

      if (loader) {
        this.home.loading = false;
      }
    });
  }

  private clearSpotlight() {
    this.El.spotlightVideo.innerHTML = '';
  }
}
