export class HomeElements {

  private _self: HTMLElement;
  private _loader: HTMLElement;
  private _spotlight: HTMLElement;
  private _spotlightInfo: HTMLElement;
  private _spotlightVideo: HTMLElement;
  private _spotlightTitle: HTMLElement;
  private _spotlightViews: HTMLElement;
  private _spotlightDate: HTMLElement;
  private _spotlightDescription: HTMLElement;
  private _videos: HTMLElement;
  private _videosButton: HTMLElement;
  private _videosLoading: HTMLElement;

  constructor() {
    this._self = document.querySelector('.js--home');
    this._loader = document.querySelector('.js--home-loader');
    this._spotlight = document.querySelector('.js--home-spotlight');
    this._spotlightInfo = document.querySelector('.js--home-spotlight-info');
    this._spotlightVideo = document.querySelector('.js--home-spotlight-video');
    this._spotlightTitle = document.querySelector('.js--home-spotlight-title');
    this._spotlightViews = document.querySelector('.js--home-spotlight-views');
    this._spotlightDate = document.querySelector('.js--home-spotlight-date');
    this._spotlightDescription = document.querySelector('.js--home-spotlight-description');
    this._videos = document.querySelector('.js--home-videos');
    this._videosButton = document.querySelector('.js--home-cards-button');
    this._videosLoading = document.querySelector('.js--home-cards-loading');
  }

  get self() {
    return this._self;
  }

  get loader() {
    return this._loader;
  }

  get spotlight() {
    return this._spotlight;
  }

  get spotlightInfo() {
    return this._spotlightInfo;
  }

  get spotlightVideo() {
    return this._spotlightVideo;
  }

  get spotlightTitle() {
    return this._spotlightTitle;
  }

  get spotlightViews() {
    return this._spotlightViews;
  }

  get spotlightDate() {
    return this._spotlightDate;
  }

  get spotlightDescription() {
    return this._spotlightDescription;
  }

  get videos() {
    return this._videos;
  }

  get videosButton() {
    return this._videosButton;
  }

  get videosLoading() {
    return this._videosLoading;
  }
}
