import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { SearchElements } from './search.element';
import { YoutubeService } from 'src/app/core/youtube/youtube.service';
import { ModalService } from 'src/app/core/modal/modal.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  private El: SearchElements;
  private channelName = 'TEDtalksDirector';
  private subscription: Subscription;
  private paramsObservable: Observable<ParamMap>;

  search = {
    dataVideos: [],
    spotlight: null,
    loading: false,
    nextPageToken: '',
    searchTerm: '',
    emptySearch: true
  }

  constructor(private youtubeService: YoutubeService, private modalService: ModalService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.El = new SearchElements();
    // this.paramsObservable = this.route.queryParamMap;
    this.subscription = this.route.queryParamMap.subscribe((data: any) => {
      this.search.searchTerm = data.params.q;

      if (this.search.searchTerm) {
        this.search.dataVideos = [];
        this.getVideos();
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  loadMoreVideos() {
    this.El.gridButton.classList.add('is--hidden');
    this.El.gridLoading.classList.add('is--active');

    this.getVideos(12, false, this.search.nextPageToken);
  }

  openModal(video: any) {
    this.modalService.build(video);
  }

  private getVideos(maxResults: number = 12, loader: boolean = true, nextPageToken: string = '') {
    if (loader) {
      this.search.loading = true;
    }

    this.youtubeService.getVideos(this.channelName, maxResults, nextPageToken, this.search.searchTerm)
    .then(data => {
      if (data.items.length === 0) {
        this.search.emptySearch = true;
      } else {
        this.search.emptySearch = false;
      }

      this.search.nextPageToken = data.nextPageToken;
      data.items.forEach(video => {
        console.log('video', video)
        this.search.dataVideos.push(video);
      });

      if (this.El.gridLoading.classList.contains('is--active')) {
        this.El.gridButton.classList.remove('is--hidden');
        this.El.gridLoading.classList.remove('is--active');
      }

      if (loader) {
        this.search.loading = false;
      }
    });
  }
}
