export class SearchElements {

  private _self: HTMLElement;
  private _loader: HTMLElement;
  private _container: HTMLElement;
  private _gridButton: HTMLElement;
  private _gridLoading: HTMLElement;

  constructor() {
    this._self = document.querySelector('.js--search');
    this._container = document.querySelector('.js--search-container');
    this._loader = document.querySelector('.js--search-loader');
    this._gridButton = document.querySelector('.js--search-grid-button');
    this._gridLoading = document.querySelector('.js--search-grid-loading');
  }

  get self() {
    return this._self;
  }

  get container() {
    return this._container;
  }

  get loader() {
    return this._loader;
  }

  get gridButton() {
    return this._gridButton;
  }

  get gridLoading() {
    return this._gridLoading;
  }
}
