import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HeaderComponent } from './common/header/header.component';
import { HomeComponent } from './home/home.component';
import { VideosComponent } from './videos/videos.component';
import { RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { ViewsPipe } from '../pipes/views.pipe';

@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent,
    VideosComponent,
    SearchComponent,
    ViewsPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  exports: [
    HeaderComponent
  ]
})
export class ComponentsModule { }
