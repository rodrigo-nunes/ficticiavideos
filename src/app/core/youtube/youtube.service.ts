import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map, tap } from 'rxjs/operators';
import { stringify } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})

export class YoutubeService {

  private url = 'https://www.googleapis.com/youtube/v3';

  constructor(private http: HttpClient) { }

  async getVideos(channelName: string, resultsPerPage: number = 5, pageToken: string = '', q: string = '', order: string = 'date') {

    const idChannel: any = await this.getId(channelName);
    const idVideos = await this.getIdVideos(idChannel.id, resultsPerPage, pageToken, q, order);
    const dataVideos = await this.getDataVideos(idVideos.items);

    return {
      nextPageToken: idVideos.nextPageToken,
      items: dataVideos
    };
  }

  getId(forUsername: string) {
    const params = new HttpParams()
      .set('part', 'id')
      .set('forUsername', forUsername)
      .set('key', environment.youtube.key)
      .set('fields', 'items(id)')
      .set('prettyPrint', 'false');

    return this.http.get<any>(`${this.url}/channels`, {params})
      .pipe(
        map(data => data.items[0])
      ).toPromise();
  }

  getIdVideos(channelId: string, resultsPerPage: number = 5, pageToken: string = '', q: string = '', order) {
    const params = new HttpParams()
      .set('part', 'id,snippet')
      .set('channelId', channelId)
      .set('key', environment.youtube.key)
      .set('q', q)
      .set('maxResults', resultsPerPage.toString())
      .set('pageToken', pageToken)
      .set('type', 'video')
      .set('order', order)
      .set('fields', 'nextPageToken,pageInfo(totalResults,resultsPerPage),items(id(videoId))')
      .set('prettyPrint', 'false');

    return this.http.get<any>(`${this.url}/search`, {params})
      .pipe(
        map(data => {
          const ids: Array<string> = [];
          
          data.items.forEach(item => ids.push(item.id.videoId));

          return {
            items: ids,
            nextPageToken: data.nextPageToken
          };
        })
      )
      .toPromise();
  }

  getDataVideos(idVideos: Array<string>): any {
    const params = new HttpParams()
      .set('part', 'snippet,statistics,player')
      .set('key', environment.youtube.key)
      .set('id', idVideos.join(','))
      // tslint:disable-next-line:max-line-length
      .set('fields', 'items(id,snippet(publishedAt,title,description,thumbnails(default(url,width,height),medium(url,width,height),high(url,width,height),standard(url,width,height))),statistics(viewCount))')
      .set('prettyPrint', 'false');

    return this.http.get<any>(`${this.url}/videos`, {params})
      .pipe(
        map(data => Object.values(data)[0])
      ).toPromise();
  }
}
