export class ModalElements {

  private _self: HTMLElement;
  private _container: HTMLElement;
  private _wrap: HTMLElement;
  private _close: HTMLElement;
  private _video: HTMLElement;
  private _title: HTMLElement;
  private _views: HTMLElement;
  private _date: HTMLElement;
  private _description: HTMLElement;

  constructor() {
    this._self = document.querySelector('.js--modal');
    this._container = document.querySelector('.js--modal-container');
    this._wrap = document.querySelector('.js--modal-wrap');
    this._close = document.querySelector('.js--modal-close');
    this._video = document.querySelector('.js--modal-video');
    this._title = document.querySelector('.js--modal-title');
    this._views = document.querySelector('.js--modal-views');
    this._date = document.querySelector('.js--modal-date');
    this._description = document.querySelector('.js--modal-description');
  }

  get self() {
    return this._self;
  }

  get container() {
    return this._container;
  }

  get wrap() {
    return this._wrap;
  }

  get close() {
    return this._close;
  }

  get video() {
    return this._video;
  }

  get title() {
    return this._title;
  }

  get views() {
    return this._views;
  }

  get date() {
    return this._date;
  }

  get description() {
    return this._description;
  }
}
