import { Injectable } from '@angular/core';
import { $modalTemplate } from './modal.template';
import { ModalElements } from './modal.elements';
import { HelpersService } from '../helpers/helpers.service';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private _el: ModalElements;
  private _modal: Element;

  constructor(private hs: HelpersService) {
    this._createModal();
  }

  build(video: any) {
    const iframe = this.hs.createIframe(video.id);
    const date = this.hs.getDateFull(video.snippet.publishedAt);
    const views = this.hs.getFormattedNumber(video.statistics.viewCount);
    
    document.body.classList.add('has--no-scroll');
    document.body.appendChild(this._modal);

    this._el = new ModalElements();
    this._el.container.classList.add('slide');
    this._el.wrap.classList.remove('has--overflow');
    this._el.close.onclick = this._closeModal.bind(this);
    this._el.video.innerHTML = '';
    this._el.video.appendChild(iframe);
    this._el.title.innerText = video.snippet.title;
    this._el.views.innerText = `${views} views`;
    this._el.date.innerText = date;
    this._el.description.innerText = video.snippet.description;

    this._checkHeight();

    setTimeout(() => this._el.container.classList.remove('slide'), 100);
  }

  private _createModal() {
    const _el = new DOMParser().parseFromString($modalTemplate, 'text/html');
    this._modal = _el.body.firstElementChild;
  }

  private _closeModal() {
    this._el.container.classList.add('slide');

    setTimeout(() => {
      this._el.container.classList.remove('slide');
      this._el.self.remove();
      document.body.classList.remove('has--no-scroll');
    }, 300);
  }

  private _checkHeight() {
    const windowHeight = window.innerHeight;
    const modalHeight = this._el.wrap.clientHeight;

    if (modalHeight >= windowHeight) {
      this._el.wrap.classList.add('has--overflow');
    } else {
      this._el.wrap.classList.remove('has--overflow');
    }
  }
}
