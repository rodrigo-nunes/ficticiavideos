export const $modalTemplate = `
  <section class="fv-modal js--modal">
    <div class="fv-modal__container js--modal-container slide">
      <div class="fv-modal__wrap js--modal-wrap">
        <div class="fv-modal__heading">
          <span class="fv-modal__heading--close js--modal-close">&times;</span>
        </div>
        <div class="fv-modal__body">
          <div class="fv-modal__body--video js--modal-video"></div>
          <div class="fv-modal__body--info">
            <h3 class="fv-modal__body--info-title js--modal-title"></h3>
            <div class="fv-modal__body--info-data">
              <div class="fv-modal__body--info-views">
                <img src="assets/img/icons/view-blue.png" alt="Visualizações"/>
                <div class="fv-modal__body--info-views-text js--modal-views"></div>
              </div>
              <div class="fv-modal__body--info-date">
                <img src="assets/img/icons/date.png" alt="Data Publicação"/>
                <div class="fv-modal__body--info-date-text js--modal-date"></div>
              </div>
            </div>
            <p class="fv-modal__body--info-description js--modal-description"></p>
          </div>
        </div>
      </div>
    </div>
  </section>
`;