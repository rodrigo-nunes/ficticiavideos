import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class HelpersService {

  constructor() { }

  getDateFull(date: string): string {
    const _date = new Date(date);
    const _day = _date.getDate();
    const _month = _date.getMonth();
    const _year = _date.getFullYear();

    return `${_day} de ${this.getMonthName(_month)} de ${_year}`;
  }

  getFormattedNumber(number: number) {
    return Number(number).toLocaleString('pt-BR');
  }

  createElement(element: string,attrs: Array<any>,parentNode: HTMLElement = null,textContent: string = '',events: Object = {}) {
    let e = document.createElement(element);
    e.textContent = textContent;

    if (attrs && attrs.length) {
      attrs.forEach((attribute) => {
        let key: any = Object.keys(attribute)[0];
        let values: any = Object.values(attribute)[0];

        if (Array.isArray(values)) {
          values.forEach((value) => {
            e.setAttribute(key, value);
          });
        } else {
          e.setAttribute(key, values);
        }
      });
    }

    if (events) {
      let event = Object.keys(events)[0];
      let callback = Object.values(events)[0];
      e.addEventListener(event, callback);
    };

    if (parentNode) parentNode.appendChild(e);

    return e;
  }

  createIframe(idVideo) {
    return this.createElement('iframe',[
      {src: `//www.youtube.com/embed/${idVideo}`},
      {width: '100%'},
      {height: '310px'},
      {frameborder: '0'},
      {allow: 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'},
      {allowfullscreen: 'allowfullscreen'}
    ]);
  }

  private getMonthName(monthNumber: number): string {
    switch(monthNumber) {
      case 0: return 'janeiro';
      case 1: return 'fevereiro';
      case 2: return 'março';
      case 3: return 'abril';
      case 4: return 'maio';
      case 5: return 'junho';
      case 6: return 'julho';
      case 7: return 'agosto';
      case 8: return 'setembro';
      case 9: return 'outrubro';
      case 10: return 'novembro';
      case 11: return 'dezembro';
    }
  }
}
