import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './components/components.module';
import { HttpClientModule } from '@angular/common/http';
import { YoutubeService } from './core/youtube/youtube.service';
import { HelpersService } from './core/helpers/helpers.service';
import { ModalService } from './core/modal/modal.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ComponentsModule
  ],
  providers: [
    YoutubeService,
    HelpersService,
    ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
