import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'views'
})
export class ViewsPipe implements PipeTransform {

  transform(value: any): string {
    const _value = Number(value);

    if ((_value / 1000 | 0) > 0) {
      return `${_value / 1000 | 0}k`
    } else if ((_value / 1000000 | 0) > 0) {
      return `${_value / 1000 | 0}m`
    } else {
      return value;
    }
  }
}
